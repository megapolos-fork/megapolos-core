/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

export interface MegapolosEvent {
  type: string;
  data: any;
}