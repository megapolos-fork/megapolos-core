/**
 * Error codes which can be retried
 */
export const RETRYABLE_ERROR_CODES: Set<string>;
/**
 * HTTP status codes which can be retried
 */
export const RETRYABLE_STATUS_CODES: Set<number>;
/**
 * HTTP methods which can be retried
 */
export const RETRYABLE_HTTP_METHODS: Set<string>;
