/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { spawnSync } from 'child_process';
import { writeFileSync, mkdirSync, readFileSync, rmSync, existsSync, rmdirSync } from 'fs';
import { sleep } from '..';
import rqlite from '../coreRqlite';

(async () => {
    const exec = commands => commands.map(command => spawnSync(command, {
        shell: true,
        stdio: 'inherit',
    }));
    const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

    const db = readFileSync(__dirname + '/newrqlite.sql').toString('utf-8');

    try {
        await sleep(4000);
        await rqlite.execute(db);
    } catch (e) {
        console.error(e);
    }
})();
