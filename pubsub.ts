/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { PubSub } from 'graphql-subscriptions';
const pubsub = new PubSub();
export default pubsub;

